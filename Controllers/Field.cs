namespace SQLMassive{
    public class Field{

        public Field(){

        }
        public Field(int id, string nome, string tipo){
            Id = id;
            Nome = nome;
            Tipo = tipo;
        }
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
    }
}